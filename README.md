# ModBridge Modbus TCP to Modbus RTU Converter for TriVibe Sensor Communication to PLC  
[Manual](https://www.moxa.com/getmedia/c99d0897-cfd2-4f45-80f3-a9247b89fc14/moxa-mgate-mb3180-mb3280-mb3480-series-manual-v11.2.pdf)  
[Gateway Finder Utility](https://www.moxa.com/getmedia/7c3ac43f-1b70-4e39-babc-80285e30741c/moxa-device-search-utility-v2.3.zip)  

## Sensor Quick Registers  
| Value | Offset/Start Address | Number of Registers | Data Type | Measureand | Adjustment to Scale |
| ----- | ------ | ------------------- | --------- | ---------- | ------------------- |
| Serial Number | 26 | 2 | Unsigned Integer | n/a | None |
| Uptime - Minutes | 5 | 1 | Unsigned Integer | n/a | None |
| Uptime - Hours | 6 | 1 | Unsigned Integer | n/a | None |
| Uptime - Days | 7 | 1 | Unsigned Integer | n/a | None |
| Temperature | 31 | 1 | Signed Integer | Celsius | Return Value / 10 |
| Axis 1 - Acceleration | 172 | 2 | Floating Point | g (RMS) | None |
| Axis 2 - Acceleration | 174 | 2 | Floating Point | g (RMS) | None |
| Axis 3 - Acceleration | 176 | 2 | Floating Point | g (RMS) | None |
| Axis 1 - Velocity | 178 | 2 | Floating Point | ips (RMS) | None |
| Axis 2 - Velocity | 180 | 2 | Floating Point | ips (RMS) | None |
| Axis 3 - Velocity | 182 | 2 | Floating Point | ips (RMS) | None |